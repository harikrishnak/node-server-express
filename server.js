
const express = require('express');
const hbs = require('hbs');
const fs = require('fs');
const port= process.env.PORT||3000;

var app = express();

hbs.registerPartials(__dirname+'/views/partials');
hbs.registerHelper('getCurrentYear',()=>{return new Date().getFullYear();})
hbs.registerHelper('changeCase',(text)=>{return text.toUpperCase();})


app.set('view engine','hbs');

//express middleware

app.use((request,response,next)=>{
   
    var isMaintenance=false;
    var logMessage=`${new Date().toString()}: method: ${request.method}, url:${request.url} \n`;

    fs.appendFile('expressLog.txt',logMessage,(err)=>{
        if(err){console.log(err);}
    });
     if(isMaintenance){
         response.render('maintenance.hbs');
     }else{
      next();
     }
}).use(express.static(__dirname+'/assets'));

app.get('/',(request,response)=>{
   response.render('home.hbs',{
       pageTitle:'Home page',
       welcomeMessage:'Welcome to book store!'
   })
}).get('/about',(request,response)=>{
    response.render('about.hbs',{
        pageTitle:'About page'
    })
}).get('/bad',(request,response)=>{
    response.send({
        errorMessage:'Something went wrong!'
    })
}).listen(port,()=>{
    console.log(`Server is up on ${port}.`)
});